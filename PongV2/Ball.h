#pragma once
#include "stdafx.h"
#include <SFML\Graphics.hpp>

class Ball
{
public:
	Ball();
	void SetStart();
	sf::FloatRect GetBounds();
	sf::CircleShape GetShape();
	void SetShape(sf::Vector2f vect2);
	void MoveBall(sf::Time &deltaTime);
	sf::Vector2f GetSpeed();
	float GetSpeedFloat();
	void SetSpeed(int change, char axis);
	void SetSpeedFloat(float change);
	void SetPosition(sf::Vector2f pos);
	void SetAngle(float angle, float offset);
	void StartBallMove();

private:
	sf::CircleShape m_shape;
	float speedFloat;
	sf::Vector2f m_ballSpeed{ speedFloat / 2, speedFloat / 2 };
};

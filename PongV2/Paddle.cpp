#include "stdafx.h"
#include "Paddle.h"
#include <SFML/Graphics.hpp>

Paddle::Paddle()
{
	m_Paddle.setSize(paddleSize);
	m_Paddle.setOrigin((sf::Vector2f(paddleWidth / 2, paddleHeight / 2)));
}

sf::RectangleShape Paddle::GetShape() {
	return m_Paddle;
}

void Paddle::SetPosition(sf::Vector2f pos) {
	m_Paddle.setPosition(pos);
}

float Paddle::GetSpeed() {
	return playerSpeed;
}

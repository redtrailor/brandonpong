#pragma once
#include "stdafx.h"
#include <SFML/Graphics.hpp>
#include "Ball.h"
#include "Paddle.h"
#include <sstream>

class Game {
public:
	Game();

	void Loop(sf::RenderWindow *window, sf::Time &deltaTime);
	void Draw(sf::RenderWindow *window);
	void CheckBounds(sf::Vector2f *position, const float paddleHeight);
	void PaddleHit(Ball *ball, sf::RectangleShape *player);
	void CheckScore(Ball *ball);

	const float windowWidth{ 1024 };
	const float windowHeight{ 576 };

private:
	enum GameState {
		STARTSCREEN,
		SPACETOSTART,
		PLAYING,
		WINNER
	};
	GameState gameState;
	Paddle m_player1;
	Paddle m_player2;
	Ball m_ball;

	sf::RectangleShape top;
	sf::RectangleShape bottom;
	sf::RectangleShape middleLine;

	const sf::Vector2f player1Start{ 25, 290 };
	const sf::Vector2f player2Start{ 999, 290 };
	
	const int borderSize{ 20 };
	
	const double PI{ 3.14159 };

	int pointOfImpact;
	int impactAngle;

	int p1Score = 0;
	int p2Score = 0;
	sf::Font font;
	sf::Text p1Display;
	sf::Text p2Display;
	sf::String space;
	sf::Text spaceToStart;
	sf::String p1wins;
	sf::String p2wins;
	
	sf::Text showWinner;

};
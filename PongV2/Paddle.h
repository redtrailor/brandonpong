#pragma once
#include "stdafx.h"
#include <SFML/Graphics.hpp>

class Paddle
{
public:
	Paddle();
	sf::RectangleShape GetShape();
	void SetPosition(sf::Vector2f pos);
	float GetSpeed();

private:
	const float paddleWidth{ 10 };
	const float paddleHeight{ 80 };
	const sf::Vector2f paddleSize{ paddleWidth, paddleHeight };
	const float playerSpeed{ 350.f };
	sf::RectangleShape m_Paddle;
};
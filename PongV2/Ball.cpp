#include "stdafx.h"
#include <SFML\Graphics.hpp>
#include "Ball.h"



Ball::Ball() :
	speedFloat(500),
	m_shape(15)
{
	m_shape.setOrigin(sf::Vector2f(m_shape.getRadius(), m_shape.getRadius()));
	sf::Vector2f m_ballSpeed{ speedFloat / 2, speedFloat / 2 };
}

void Ball::SetStart() {
	//sets the balls position back to center of the window.
	m_shape.setPosition(sf::Vector2f(512, 288));
}

sf::FloatRect Ball::GetBounds() {
	return m_shape.getGlobalBounds();
}

sf::CircleShape Ball::GetShape() {
	return m_shape;
}

void Ball::SetShape(sf::Vector2f vect2) {

}

void Ball::MoveBall(sf::Time &deltaTime) {
	m_shape.move(m_ballSpeed * deltaTime.asSeconds());
}

sf::Vector2f Ball::GetSpeed() {
	return m_ballSpeed;
}

float Ball::GetSpeedFloat() {
	return speedFloat;
}

void Ball::SetSpeed(int change, char axis) {
	if (axis == 'x') {
		m_ballSpeed.x = (m_ballSpeed.x * change);
	}
	else {
		m_ballSpeed.y = (m_ballSpeed.y * change);
	}
}

void Ball::SetSpeedFloat(float change) {
	speedFloat += change;
}

void Ball::SetPosition(sf::Vector2f pos) {
	m_shape.setPosition(pos);
}

void Ball::SetAngle(float angle, float offset) {
	m_ballSpeed.y = angle;
	m_ballSpeed.x = offset;

}

void Ball::StartBallMove() {
	sf::Vector2f m_ballSpeed{ speedFloat / 2, speedFloat / 2 };
}
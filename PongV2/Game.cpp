#include "stdafx.h"
#include <SFML\Graphics.hpp>
#include "Game.h"
#include "Ball.h"
#include "Paddle.h"

Game::Game() {
	gameState = STARTSCREEN;

	m_player1.SetPosition(player1Start);
	m_player2.SetPosition(player2Start);
	m_ball.SetStart();
	m_ball.StartBallMove();

	top.setPosition(borderSize, 0);
	top.setSize(sf::Vector2f(windowWidth - (2 * borderSize), borderSize));
	top.setFillColor(sf::Color::White);

	bottom.setPosition(borderSize, windowHeight - borderSize);
	bottom.setSize(sf::Vector2f(windowWidth - (2 * borderSize), borderSize));
	bottom.setFillColor(sf::Color::White);

	middleLine.setPosition((windowWidth / 2), 0);
	middleLine.setSize(sf::Vector2f(5, windowHeight));
	middleLine.setFillColor(sf::Color::White);

	if (!font.loadFromFile("Media/consola.ttf")) {
		printf("Error loading Font");
	}
	spaceToStart.setColor(sf::Color::Green);
	spaceToStart.setString(sf::String("Please Press Space To Begin" + p1wins));
	spaceToStart.setCharacterSize(40);
	spaceToStart.setFont(font);
	spaceToStart.setPosition(sf::Vector2f(220, 250));

	p1Display.setColor(sf::Color(150,150,150));
	p1Display.setString(sf::String(std::to_string(p1Score)));
	p1Display.setCharacterSize(100);
	p1Display.setFont(font);
	p1Display.setPosition(sf::Vector2f(300, 50));

	p2Display.setColor(sf::Color(150, 150, 150));
	p2Display.setString(sf::String(std::to_string(p2Score)));
	p2Display.setCharacterSize(100);
	p2Display.setFont(font);
	p2Display.setPosition(sf::Vector2f(windowWidth - 300, 50));

	p1wins = "Player 1 ";
	p2wins = "Player 2 ";
	showWinner.setColor(sf::Color::Green);
	showWinner.setCharacterSize(40);
	showWinner.setFont(font);
	showWinner.setPosition(sf::Vector2f(275, 190));
}

void Game::Loop(sf::RenderWindow *window, sf::Time &deltaTime) {
	sf::Event event;
	while (window->pollEvent(event)) {
		if (event.type == sf::Event::Closed) {
			window->close();
		}
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
		window->close();
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
		if (gameState == STARTSCREEN || WINNER) {
			gameState == PLAYING;
			if (gameState == WINNER) {
				p1Score = 0;
				p2Score = 0;
				m_ball.SetStart();
				m_ball.StartBallMove();
			}
		}
		if (gameState == SPACETOSTART)
			m_ball.SetStart();
		gameState = PLAYING;
		
	}

	//////////////////////////////////////////////
	//  
	//		Player 1 controlling
	//
	/////////////////////////////////////////////
	sf::Vector2f position1(m_player1.GetShape().getPosition());
	float movement1(m_player1.GetSpeed() * deltaTime.asSeconds());
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
		position1.y -= movement1;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
		position1.y += movement1;
	}
	CheckBounds(&position1, m_player1.GetShape().getSize().y);
	m_player1.SetPosition(position1);

	///////////////////////////////////////////
	//
	//			Player 2 controls
	//
	///////////////////////////////////////////
	sf::Vector2f position2(m_player2.GetShape().getPosition());
	float movement2(m_player2.GetSpeed() * deltaTime.asSeconds());
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
		position2.y -= movement1;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
		position2.y += movement1;
	}
	CheckBounds(&position2, m_player2.GetShape().getSize().y);
	m_player2.SetPosition(position2);

	if (gameState == PLAYING) 
		m_ball.MoveBall(deltaTime);

	if (m_ball.GetShape().getGlobalBounds().intersects(bottom.getGlobalBounds()) ||
		m_ball.GetShape().getGlobalBounds().intersects(top.getGlobalBounds())) {
			m_ball.SetSpeed(-1,'y');
			if (m_ball.GetShape().getGlobalBounds().intersects(top.getGlobalBounds())) {
				m_ball.SetPosition(sf::Vector2f(m_ball.GetShape().getPosition().x, borderSize + m_ball.GetShape().getRadius()));
			}
			else {
				m_ball.SetPosition(sf::Vector2f(m_ball.GetShape().getPosition().x, (windowHeight - borderSize - m_ball.GetShape().getRadius())));
			}
	}
	PaddleHit(&m_ball, &m_player1.GetShape());
	PaddleHit(&m_ball, &m_player2.GetShape());

	if (gameState == PLAYING)
		CheckScore(&m_ball);

}

void Game::CheckBounds(sf::Vector2f *position, const float paddleHeight) {
	if (position->y < borderSize + paddleHeight / 2) {
		position->y = borderSize + paddleHeight / 2;
	}
	else if (position->y >(windowHeight - borderSize - paddleHeight / 2)) {
		position->y = (windowHeight - borderSize - paddleHeight / 2);
	}
}

void Game::PaddleHit(Ball *ball, sf::RectangleShape *player) {

	float tempAngle;
	float tempOffset;

	if (ball->GetBounds().intersects(player->getGlobalBounds())) {
		pointOfImpact = (ball->GetShape().getPosition().y - player->getPosition().y);
		impactAngle = (pointOfImpact + pointOfImpact);
		tempAngle = sin(impactAngle * PI / 180)*m_ball.GetSpeedFloat();
		if (ball->GetShape().getPosition().x < player->getPosition().x) {
			tempOffset = sqrtf(powf(ball->GetSpeedFloat(), 2) - powf(tempAngle, 2));
		}
		else {
			tempOffset = -sqrtf(powf(ball->GetSpeedFloat(), 2) - powf(tempAngle, 2));
		}

		ball->SetAngle(tempAngle, tempOffset);
		ball->SetSpeed(-1, 'x');
		ball->SetSpeedFloat(10);
	}
}
void Game::CheckScore(Ball *ball) {
	if (ball->GetShape().getPosition().x < 0) {
		p2Score++;
		ball->SetStart();
		gameState = SPACETOSTART;
	}
	else if (ball->GetShape().getPosition().x > 1024) {
		p1Score++;
		ball->SetStart();
		gameState = SPACETOSTART;
	}
	p1Display.setString(sf::String(std::to_string(p1Score)));
	p2Display.setString(sf::String(std::to_string(p2Score)));

	if (p1Score >= 10) {
		gameState = WINNER;
		showWinner.setString(p1wins + "is the winner!");
	}
	else if (p2Score >= 10) {
		gameState = WINNER;
		showWinner.setString(p2wins + "is the winner!");
	}

}

void Game::Draw(sf::RenderWindow *window) {

	window->clear();

	switch (gameState) {
	case STARTSCREEN:
		window->draw(spaceToStart);
		break;
	case SPACETOSTART:
		window->draw(m_player1.GetShape());
		window->draw(m_player2.GetShape());
		window->draw(middleLine);
		window->draw(top);
		window->draw(bottom);
		window->draw(p1Display);
		window->draw(p2Display);
		window->draw(spaceToStart);
		break;
	case PLAYING:
		window->draw(m_player1.GetShape());
		window->draw(m_player2.GetShape());
		window->draw(middleLine);
		window->draw(top);
		window->draw(bottom);
		window->draw(p1Display);
		window->draw(p2Display);
		window->draw(m_ball.GetShape());
		break;
	case WINNER:
		window->draw(m_player1.GetShape());
		window->draw(m_player2.GetShape());
		window->draw(top);
		window->draw(bottom);
		window->draw(p1Display);
		window->draw(p2Display);
		window->draw(showWinner);
		window->draw(spaceToStart);

		break;
	}
	window->display();
}
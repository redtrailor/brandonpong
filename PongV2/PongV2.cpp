// PongV2.cpp : Defines the entry point for the console application.

#include "stdafx.h"
#include <cmath>
#include <SFML/Graphics.hpp>
#include "Game.h"

int _tmain(int argc, _TCHAR* argv[])
{
	Game myGame;
	sf::Clock clock;

	sf::RenderWindow window(sf::VideoMode(myGame.windowWidth, myGame.windowHeight), "PONG");
	window.setMouseCursorVisible(false);
	window.setFramerateLimit(120);

	while (window.isOpen()) {

		sf::Time deltaTime(clock.restart());
		myGame.Loop(&window, deltaTime);
		myGame.Draw(&window);
	}
	return 0;
}

